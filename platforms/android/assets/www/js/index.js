var menuIsOpen = false;
var pages = null;
var currentPage = "page1";
var loadPages;
loadPages = function () {
    "use strict";
    pages = {
        page1: document.getElementById("Page-1"),
        page2: document.getElementById("Page-2"),
        salir: document.getElementById("Salir")
    };
};

var events = {
    /*
    Eventos de una app en PhoneGap
    --------------------------------
    deviceready
    pause
    resume
    backbutton
    menubutton
    searchbutton
    startcallbutton
    endcallbutton
    volumedownbutton
    volumeupbutton
*/
    
    deviceReady: function () {
        "use strict";
        console.log("Aplicación iniciada");
        statusdiv = document.getElementById("BattStatus");
        window.addEventListener("batterycritical", battCrit, false);
        window.addEventListener("batterylow", battLow, false);
        window.addEventListener("batterystatus", battStat, false);
    },
    // evento disparado cuando todo el HTML ha sido cargado
    contentLoaded: function () {
        "use strict";
        loadPages();
        FastClick.attach(document.body);
    },
    backButton: function () {
        "use strict";
        // navigator.app.exitApp();
    }
};

// listener evento de dispositivo listo
document.addEventListener('deviceready', events.deviceReady, false);
// listener de contenido DOM listo
document.addEventListener('DOMContentLoaded', events.contentLoaded, false);

// función click en menú
function menuButtonClick() {
    "use strict";
    if (menuIsOpen) {
        pages[currentPage].className = "body transition center";
        menuIsOpen = false;
        console.log(menuIsOpen);
    } else {
        pages[currentPage].className = "body transition right";
        menuIsOpen = true;
        console.log(menuIsOpen);
    }
}

// función cambiar página
function changePage(pageSelected) {
    "use strict";
    console.log(pageSelected);
    
    pages[currentPage].className = "hide";
    pages[pageSelected].className = "body right";
    setTimeout(function () {
        pages[pageSelected].className = "body visible transition center";
        currentPage = pageSelected;
        menuIsOpen = false;
        console.log(currentPage);
        console.log(menuIsOpen);
    }, 0.25);
}

function onSuccess(position) {
    var element = document.getElementById('geolocation');
    element.innerHTML = 'Latitude: '  + position.coords.latitude      + '<br />' +
                        'Longitude: ' + position.coords.longitude     + '<br />' +
                        '<hr />'      + element.innerHTML;
    console.log("ha entrado gps");
}

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

var battCrit = function(info) {
    navigator.notification.alert("Your battery is SUPER low!");
    drawStatus(info);
};

var battLow = function(info) {
    navigator.notification.alert("Your battery is low!");
    drawStatus(info);
};

    var battStat = function(info) {
drawStatus(info);
};
